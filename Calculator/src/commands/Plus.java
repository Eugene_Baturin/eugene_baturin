package commands;

import calc.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Plus implements Command {

    private static final Logger LOGGER = Logger.getLogger(Plus.class.getName());

    public void execute(List<String> curStr, Context curCont){
        try{
            if (curStr.size() == 1) {
                Double topSum = (Double)curCont.getStack().pop()+(Double)curCont.getStack().pop();
                curCont.getStack().push(topSum);
                LOGGER.log(Level.INFO, "PLUS executed successfully: the result is " + topSum);
            }
            throw new Exception("PLUS-command was typed incorrectly");
        }catch(Exception ex){
            LOGGER.log(Level.SEVERE, "Exception was caught in PLUS", ex);
            System.out.println("ERROR in commands.Plus:" + ex);
        }
    }

}
