package commands;

import calc.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Minus implements Command {

    private static final Logger LOGGER = Logger.getLogger(Minus.class.getName());

    public void execute(List<String> curStr, Context curCont){
        try{
            if (curStr.size() == 1) {
                Double topMin = -(Double) curCont.getStack().pop() + (Double) curCont.getStack().pop();
                curCont.getStack().push(topMin);
                LOGGER.log(Level.INFO, "MINUS executed successfully: the result is " + topMin);
            }
            else throw new Exception();
        }catch(Exception ex){
            LOGGER.log(Level.INFO, "MINUS executed successfully");
            System.out.println("ERROR in commands.Minus:" + ex);
        }
    }
}
