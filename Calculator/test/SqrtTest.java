import calc.Context;
import commands.Sqrt;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SqrtTest {

    @Test
    public void testSqrt() {
        try {
            Context testCont = new Context();
            Sqrt testClass = new Sqrt();
            List<String> curStr1 = new ArrayList<>();
            curStr1.add("SQRT");

            testCont.getStack().push(16.0);
            testClass.execute(curStr1, testCont);

            assertEquals(4.0, (double) testCont.getStack().peek());
        } catch (Exception ex) {
            System.out.println("ERROR in SqrtTest:" + ex);
        }
    }
}