import calc.Context;
import commands.Multyply;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MultyplyTest{

    @Test
    public void testMultyply() {
        try {
            Context testCont = new Context();
            Multyply testClass = new Multyply();
            List<String> curStr1 = new ArrayList<>();
            curStr1.add("*");

            testCont.getStack().push(5.0);
            testCont.getStack().push(4.0);
            testClass.execute(curStr1, testCont);

            assertEquals(20.0, (double) testCont.getStack().peek());
        } catch (Exception ex) {
            System.out.println("ERROR in MultyplyTest:" + ex);
        }
    }

}