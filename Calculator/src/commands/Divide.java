package commands;

import calc.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Divide implements Command {

    private static final Logger LOGGER = Logger.getLogger(Divide.class.getName());

    public void execute(List<String> curStr, Context curCont){
        try{
            if (curStr.size() == 1) {
                Double topDiv = (1.0 / (Double) curCont.getStack().pop()) * (Double) curCont.getStack().pop();
                curCont.getStack().push(topDiv);
                LOGGER.log(Level.INFO, "DIVIDE executed successfully: the result is " + topDiv);
            }
            else throw new Exception();
        }catch(Exception ex){
            LOGGER.log(Level.SEVERE, "Exception was caught in DIVIDE", ex);
            System.out.println("ERROR in commands.Divide:" + ex);
        }
    }

}
