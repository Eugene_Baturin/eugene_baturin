package commands;

import calc.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Sqrt implements Command {

    private static final Logger LOGGER = Logger.getLogger(Sqrt.class.getName());

    public void execute(List<String> curStr, Context curCont){
        try {
            if (curStr.size() == 1) {
                Double topSqrt = Math.sqrt((Double) curCont.getStack().pop());
                curCont.getStack().push(topSqrt);
                LOGGER.info("SQRT executed successfully: the result is " + topSqrt);
            }
            else throw new Exception("SQRT-command was typed incorrectly");
        }catch (Exception ex) {
            LOGGER.log(Level.SEVERE, "Exception was caught in SQRT", ex);
            System.out.println("ERROR in commands.Sqrt:" + ex);
        }
    }
}