import calc.Context;
import commands.Divide;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DivideTest {

    @Test
    public void testDivide() {
        try {
            Context testCont = new Context();
            Divide testClass = new Divide();
            List<String> curStr1 = new ArrayList<>();
            curStr1.add("/");

            testCont.getStack().push(16.0);
            testCont.getStack().push(4.0);
            testClass.execute(curStr1, testCont);

            assertEquals(4.0, (double) testCont.getStack().peek());
        } catch (Exception ex) {
            System.out.println("ERROR in DivideTest:" + ex);
        }
    }
}