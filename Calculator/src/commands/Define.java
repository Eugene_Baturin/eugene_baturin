package commands;

import calc.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Define implements Command {

    private static final Logger LOGGER = Logger.getLogger(Define.class.getName());

    public void execute(List<String> curStr, Context curCont){
        try{
            if ((curStr.size() == 3)&&!(curStr.get(1).matches("^[-]?[0-9]*[.,]?[0-9]+$"))) {
                curCont.getMap().put(curStr.get(1), Double.parseDouble(curStr.get(2)));
                LOGGER.log(Level.INFO, "DEFINE executed successfully: " + curStr.get(1) + " and " + Double.parseDouble(curStr.get(2)) + " were pushed");
            }
            else throw new Exception();
        }catch(Exception ex){
            LOGGER.log(Level.SEVERE, "Exception was caught in DEFINE", ex);
            System.out.println("ERROR in commands.Define:" + ex);
        }
    }

}
