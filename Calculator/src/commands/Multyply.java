package commands;

import calc.*;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Multyply implements Command {

    private static final Logger LOGGER = Logger.getLogger(Multyply.class.getName());

    public void execute(List<String> curStr, Context curCont){
        try{
            if (curStr.size() == 1) {
                Double topMul = (Double)curCont.getStack().pop()*(Double)curCont.getStack().pop();
                curCont.getStack().push(topMul);
                LOGGER.log(Level.INFO, "MULTYPLY executed successfully: the result is " + topMul);
            }
            else throw new Exception("MULTYPLY-command was typed incorrectly");
        }catch(Exception ex){
            LOGGER.log(Level.SEVERE, "Exception was caught in MULTYPLY", ex);
            System.out.println("ERROR in commands.Multyply:" + ex);
        }
    }

}
