package calc;

import java.util.logging.*;

public class Main{

    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(java.lang.String[] args) {
        try {
            LogManager.getLogManager().readConfiguration(Main.class.getResourceAsStream("calc/logging.properties"));
            Calculator calc = new Calculator("calc/Configuration.txt");
            calc.inputConsole(args);

        }catch (Exception ex){
            System.out.println("ERROR in Main:" + ex);
        }
    }
}


