import calc.Context;
import commands.Plus;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PlusTest {

    @Test
    public void testPlus() {
        try {
            Context testCont = new Context();
            Plus testClass = new Plus();
            List<String> curStr1 = new ArrayList<>();
            curStr1.add("+");

            testCont.getStack().push(5.0);
            testCont.getStack().push(4.0);
            testClass.execute(curStr1, testCont);

            assertEquals(9.0, (double) testCont.getStack().peek());
        } catch (Exception ex) {
            System.out.println("ERROR in PlusTest:" + ex);
        }
    }
}