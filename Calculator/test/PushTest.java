import calc.Context;
import commands.Push;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PushTest {

    @Test
    public void testPush() {
        try{
            Context testCont = new Context();
            Push testClass = new Push();
            List<String> curStr1 = new ArrayList<>();
            curStr1.add("PUSH");curStr1.add("4");

            List<String> curStr2 = new ArrayList<>();
            curStr2.add("PUSH");curStr2.add("a");

            testCont.getStack().push(5.0);
            testCont.getStack().push(6.0);
            testCont.getStack().push(7.0);
            int initialSize = testCont.getStack().size();
            testClass.execute(curStr1, testCont);
            testCont.getMap().put("a", 10.0);
            testClass.execute(curStr2,testCont);
            int changedSize = testCont.getStack().size();

            assertEquals(2, changedSize-initialSize);
            assertEquals(10.0, (double) testCont.getStack().peek());
        }catch(Exception ex){
            System.out.println("ERROR in PushTest:" + ex);
        }
    }
}